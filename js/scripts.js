jQuery(document).ready(function() {

    $('.hamburger').click(function() {
        $this = $(this);

        if ($this.hasClass('is-active')) {
            $('header').removeClass('header-z-index');
            $('body,html').removeClass("overflow");

            $this.removeClass('is-active');
            $(".overlay-contentpush").removeClass('open');
        } else {
            $this.addClass('is-active');
            $('header').addClass('header-z-index');
            $('body,html').addClass("overflow");
            $(".overlay-contentpush").addClass('open');
        }
    });

}, ($))