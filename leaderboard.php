<?php include_once('header.php'); ?>
<div class="body page leaderboard">
    <section class="content-section ">
      <div class="container">
       <h1>KRILL <br/>LEADERBOARD</h1>
       <p class="subtitle">EARN KRILL BY PLAYING SWC POKER CASH GAMES. TOP WEEKLY &amp; MONTHLY PLAYERS WIN EXTRA CHIPS!</p>
       <div class="row">
           <div class="col-lg-6 col-md-12">
            <div class="table-tournament table-poker table-responsive column-table">
                <table> 
                    <tr>
                    <th width="30%" colspan="4">LAST WEEK'S WINNERS</th>
                    </tr>
                    <tr>
                        <td width="10">#</td>
                        <td>USERNAME</td>
                        <td>KRILL</td>
                        <td>PRIZE</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>lilb1tcoin</td>
                        <td>2,143.46 Krill</td>
                        <td>5,000 Chips</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                </table>
            </div>
           </div>
           <div class="col-lg-6 col-md-12">
            <div class="table-tournament table-poker table-responsive column-table">
                <table> 
                    <tr>
                    <th width="30%" colspan="4">LAST MONTH'S WINNERS</th>
                    </tr>
                    <tr>
                        <td width="10">#</td>
                        <td>USERNAME</td>
                        <td>KRILL</td>
                        <td>PRIZE</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>lilb1tcoin</td>
                        <td>2,143.46 Krill</td>
                        <td>5,000 Chips</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                </table>
            </div>
           </div>
           <div class="col-lg-6 col-md-12">
            <div class="table-tournament table-poker table-responsive column-table yellow">
                <table> 
                    <tr>
                    <th width="30%" colspan="4">LAST WEEK'S WINNERS</th>
                    </tr>
                    <tr>
                        <td width="10">#</td>
                        <td>USERNAME</td>
                        <td>KRILL</td>
                        <td>PRIZE</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>lilb1tcoin</td>
                        <td>2,143.46 Krill</td>
                        <td>5,000 Chips</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                </table>
            </div>
           </div>
           <div class="col-lg-6 col-md-12">
            <div class="table-tournament table-poker table-responsive column-table yellow">
                <table> 
                    <tr>
                    <th width="30%" colspan="4">LAST MONTH'S WINNERS</th>
                    </tr>
                    <tr>
                        <td width="10">#</td>
                        <td>USERNAME</td>
                        <td>KRILL</td>
                        <td>PRIZE</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>lilb1tcoin</td>
                        <td>2,143.46 Krill</td>
                        <td>5,000 Chips</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>nuklang</td>
                        <td>2,043.75 Krill</td>
                        <td>3,000 Chips</td>
                    </tr>
                </table>
            </div>
           </div>
       </div>
    </div>
    </section> 
   
</div><!--end body-->
<?php include_once('footer.php'); ?>
   