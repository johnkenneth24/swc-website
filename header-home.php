<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet" >
    <title>Bitcoin Poker | Crypto Poker Tournaments</title><meta id="description" name="description" content="Play bitcoin poker on PC, Mac, Android, HTML5. Texas holdem, omaha, draw, stud, mixed. Cash games and tournaments."/><meta id="keywords" name="keywords" content="bitcoin poker, bitcoin online poker, online poker, poker, swcpoker, swc poker, swc, seals with clubs"/><meta id="og-description" property="og:description" content="Play bitcoin poker on PC, Mac, Android, HTML5. Texas holdem, omaha, draw, stud, mixed. Cash games and tournaments."/><meta id="twitter-description" name="twitter:description" content="Play bitcoin poker on PC, Mac, Android, HTML5. Texas holdem, omaha, draw, stud, mixed. Cash games and tournaments."/><meta id="twitter-card" name="twitter:card" content="summary"/><meta id="twitter-site" name="twitter:site" content="@swcpoker"/><meta id="twitter-creator" name="twitter:creator" content="@swcpoker"/><meta id="twitter-image" name="twitter:image" content="https://swcpoker.eu/api/v1/media/s7gBgZ6CRo"/><meta id="twitter-image-alt" name="twitter:image:alt"/><meta id="og-type" property="og:type" content="article"/><meta id="og-image" property="og:image" content="https://swcpoker.eu/api/v1/media/vDDmpICwrp"/><meta id="og-image-alt" property="og:image:alt"/><meta id="twitter-title" name="twitter:title" content="Bitcoin Poker | Crypto Poker Tournaments"/><meta id="og-title" property="og:title" content="Bitcoin Poker | Crypto Poker Tournaments"/><meta id="og-url" property="og:url" content="https://swcpoker.club"/><meta id="og-site-name" property="og:site_name" content="SwC Poker"/><link id="canonical" rel="canonical" href="https://swcpoker.club"/>

  </head>
  <body class="home">
  

  <header>
    <nav class="navbar navbar-expand-lg">
      <div class="container">
        <a class="swc-logo" href="./"><img src="images/logo.svg" alt=""></a>
        <button class="hamburger  hamburger--spin" type="button">
            <span class="hamburger-box" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="hamburger-inner"></span>
            </span>
        </button>
        <div class="overlay-contentpush overlay mobile-view">
            <ul>
            <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">POKER RULES</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">HOUSE RULES</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Promotion</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Updates</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Leaderboard</a>
          </li>
            </ul>
            <div class="poker-stats">
              <div class="container">
                <div class="info">
                  <div class="col-box">
                    <div class="box">
                      <h2>1,816,761
                      </h2>
                      <p>BAD BEAT JACKPOT</p>
                    </div>
                  </div>
                  <div class="col-box">
                    <div class="box">
                        <ul>
                          <li>
                          <h2>174</h2>
                          <span>Online</span>
                          </li>
                          <li>
                          <h2>62</h2>
                          <span>Players</span>
                          </li>
                          <li>
                          <h2>178</h2>
                          <span>Tables</span>
                          </li>
                        </ul>
                    </div>
                  </div>
                  <div class="col-box">
                  <h2>Trumptcus
                      </h2>
                      <p>Weekly Krill king</p>
                  </div>
                </div>
              </div>
          </div>
        </div>
       
        <div class="collapse navbar-collapse ml-auto" id="navmenu">
          <ul class="navbar-nav  mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#">POKER RULES</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">HOUSE RULES</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Promotion</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Updates</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Leaderboard</a>
            </li>

          </ul>
          <div class="get-button">
            <a href="#" class="btn btn-download">Download</a>
            <a href="#" class="btn btn-playnow">Play Now</a>
          </div>
        </div>
      </div>
      <div class="slant">
        <svg xmlns="http://www.w3.org/2000/svg" width="1933.203" height="272.982" viewBox="0 0 1933.203 272.982">
          <path id="Path_5" data-name="Path 5" d="M0,32.016v192.4H424l26.047,48.564H1470.641l27.561-48.564h435V0H0Z" fill="#033018"  />
        </svg>
      </div>
  
    </nav>
    <div class="poker-stats">
      <div class="container">
        <div class="info">
          <div class="col-box">
            <div class="box">
              <h2>1,816,761
              </h2>
              <p>BAD BEAT JACKPOT</p>
            </div>
          </div>
          <div class="col-box">
            <div class="box">
                <ul>
                  <li>
                  <h2>174</h2>
                  <span>Online</span>
                  </li>
                  <li>
                  <h2>62</h2>
                  <span>Players</span>
                  </li>
                  <li>
                  <h2>178</h2>
                  <span>Tables</span>
                  </li>
                </ul>
            </div>
          </div>
          <div class="col-box">
          <h2>Trumptcus
              </h2>
              <p>Weekly Krill king</p>
          </div>
        </div>
      </div>
    </div>
  </header>
  

