<?php include_once('header.php'); ?>
<div class="body page" id="blog">
    <section class="content-section">
      <div class="container">
       <h1>Posts</h1>
       <div class="row">
           <div class="col-lg-9 col-md-12 index-links">            
           <div class="bloglist blog-post">
              <div class="row">
                  <div class="item col-md-6 col-lg-6 col-sm-12">
                      <div class="inner">
                          <div class="featured-img">
                            <img src="images/blog-2.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,  </p>
                              <a href="#" class="btn read-more">Read More</a>
                          </div>
                      </div>
                  </div>
                  <div class="item col-md-6 col-lg-6 col-sm-12">
                      <div class="inner">
                          <div class="featured-img">
                            <img src="images/blog-1.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,  </p>
                              <a href="#" class="btn read-more">Read More</a>
                          </div>
                      </div>
                  </div>
                  <div class="item col-md-6 col-lg-6 col-sm-12">
                      <div class="inner">
                          <div class="featured-img">
                            <img src="images/blog-3.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,  </p>
                              <a href="#" class="btn read-more">Read More</a>
                          </div>
                      </div>
                  </div>
                  <div class="item col-md-6 col-lg-6 col-sm-12">
                      <div class="inner">
                          <div class="featured-img">
                            <img src="images/blog-2.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,  </p>
                              <a href="#" class="btn read-more">Read More</a>
                          </div>
                      </div>
                  </div>
          
               
              </div>
            <div class="pagination">
                <div class="pager">
                    <ul>
                        <li>
                            <a href="#">
                                <i class="fas fa-angle-double-left"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-chevron-left"></i>
                            </a>
                        </li>
                        <li class="active">
                            <a href="#">1</a>
                        </li>
                        <li>
                            <a href="#">2</a>
                        </li>
                        <li>
                            <a href="#">3</a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-chevron-right"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-angle-double-right"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
          </div>
           </div>
           <div class="col-lg-3 col-md-12 sidenav">
                <div class="widget">
                <h5>CATEGORIES</h5>
                   <ul>
                    <li><a href="#">Updates</a></li>
                    <li><a href="#">Promotions </a></li>
                    <li><a href="#">Blog </a></li>
                    <li><a href="#">Blockchain Poker Announcements </a></li>
                    <li><a href="#">Bitcoin Poker Tournaments </a></li>
                    <li><a href="#">Bitcoin Poker Freerolls </a></li>
                    <li><a href="#">Crypto Poker Promotions </a></li>
                    <li><a href="#">Crypto Poker Cash Games </a></li>
                    <li><a href="#">BTC Poker Bad Beat </a></li>
                    <li><a href="#">Blockchain Poker Strategy </a></li>
                    <li><a href="#">Cryptocurrency Gambling Industry </a></li>
                    <li><a href="#"> Bitcoin Jackpots </a></li>
                  </ul>
                </div>
                <div class="widget">
                    <h5>MORE TO READ</h5>
                   <ul>
                    <li><a href="#"> House Rules & FAQ </a></li>
                    <li><a href="#">Poker Game Rules </a></li>
                    <li><a href="#">Hand Rankings </a></li>
                    <li><a href="#">Game & Betting Styles </a></li>
                    <li><a href="#">Bitcoin FAQ </a></li>
                    <li><a href="#">Promotions </a></li>
                    <li><a href="#">Download </a></li>
                    <li><a href="#">Play Now </a></li>

                  </ul>
                </div>
                <div class="widget">
                    <h5>RECENT BLOG POSTS</h5>
                   <ul>
                    <li><a href="#">More Blogpost</a></li>
                  </ul>
                </div>
                
           </div>
       </div>
    </div>
    </section>  
 
</div><!--end body-->
<?php include_once('footer.php'); ?>
   