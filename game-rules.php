<?php include_once('header.php'); ?>
<div class="body page">
    <section class="content-section">
      <div class="container">
       <h1>POKER GAME RULES</h1>
       <div class="row">
           <div class="col-lg-8 col-md-12 index-links">            
                <div class="row ">
                    <div class="col-md-6 item">
                          <h4>Chinese Poker</h4>
                          <div class="links">
                              <ul>
                                <li> <a href="#"> Chinese Poker </a></li>
                                <li> <a href="#">Open Face Chinese (OFC) Poker </a></li>
                                <li> <a href="#">Pineapple OFC (OFC/P) Poker </a></li>
                                <li> <a href="#">Progressive Pineapple OFC Poker </a></li>
                                <li> <a href="#">2-7 Pineapple OFC Poker </a></li>
                                <li> <a href="#">7-Card Stud Poker  </a></li>
                                <li> <a href="#"> 7-Card Stud Hi/Lo Poker  </a></li>
                                <li> <a href="#"> Razz Poker</a></li>
                              </ul>
                          </div>
                    </div>
                    <div class="col-md-6 item">
                    <h4>Stud Poker Games</h4>
                          <div class="links">
                              <ul>
                                <li> <a href="#"> 7-Card Stud Poker  </a></li>
                                <li> <a href="#">7-Card Stud Hi/Lo Poker  </a></li>
                                <li> <a href="#">Razz Poker </a></li>
                              </ul>
                          </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 item">
                          <h4>Flop Games</h4>
                          <div class="links">
                              <ul>
                                <li> <a href="#"> Chinese Poker </a></li>
                                <li> <a href="#">Texas Hold'em Poker </a></li>
                                <li> <a href="#">Short Deck Hold'em Poker </a></li>
                                <li> <a href="#">Pineapple Hold'em (3-Card) Poker </a></li>
                                <li> <a href="#">Omaha Poker </a></li>
                                <li> <a href="#">Omaha Hi/Lo Poker </a></li>
                                <li> <a href="#">5-Card Omaha Poker </a></li>
                                <li> <a href="#">5-Card Omaha Hi/Lo (Big O) Poker </a></li>
                                <li> <a href="#">Courchevel Poker </a></li>
                                <li> <a href="#">Courchevel Hi/Lo Poker </a></li>
                              </ul>
                          </div>
                    </div>
                    <div class="col-md-6 item">
                        <h4>Draw Poker Games </h4>
                          <div class="links">
                              <ul>
                                <li> <a href="#"> 2-7 Triple Draw Poker </a></li>
                                <li> <a href="#"> Badugi Poker  </a></li>
                                <li> <a href="#"> Badeucy Poker  </a></li>
                                <li> <a href="#"> Badacey Poker  </a></li>
                              </ul>
                          </div>
                          <h4>Mixed Poker Games </h4>
                          <div class="links">
                              <ul>
                                <li> <a href="#"> See all Mixed Poker Games  </a></li>      
                              </ul>
                          </div>
                    </div>
                </div>
           </div>
           <div class="col-lg-4 col-md-12 sidenav">
                <div class="widget">
                <h5>CATEGORIES</h5>
                   <ul>
                    <li><a href="#">Updates</a></li>
                    <li><a href="#">Promotions </a></li>
                    <li><a href="#">Blog </a></li>
                    <li><a href="#">Blockchain Poker Announcements </a></li>
                    <li><a href="#">Bitcoin Poker Tournaments </a></li>
                    <li><a href="#">Bitcoin Poker Freerolls </a></li>
                    <li><a href="#">Crypto Poker Promotions </a></li>
                    <li><a href="#">Crypto Poker Cash Games </a></li>
                    <li><a href="#">BTC Poker Bad Beat </a></li>
                    <li><a href="#">Blockchain Poker Strategy </a></li>
                    <li><a href="#">Cryptocurrency Gambling Industry </a></li>
                    <li><a href="#"> Bitcoin Jackpots </a></li>
                  </ul>
                </div>
                <div class="widget">
                    <h5>MORE TO READ</h5>
                   <ul>
                    <li><a href="#"> House Rules & FAQ </a></li>
                    <li><a href="#">Poker Game Rules </a></li>
                    <li><a href="#">Hand Rankings </a></li>
                    <li><a href="#">Game & Betting Styles </a></li>
                    <li><a href="#">Bitcoin FAQ </a></li>
                    <li><a href="#">Promotions </a></li>
                    <li><a href="#">Download </a></li>
                    <li><a href="#">Play Now </a></li>

                  </ul>
                </div>
                <div class="widget">
                    <h5>RECENT BLOG POSTS</h5>
                   <ul>
                    <li><a href="#">More Blogpost</a></li>
                  </ul>
                </div>
                
           </div>
       </div>
    </div>
    </section>  
    <section class="section-blog latest-promition">
      <div class="container">
          <div class="title">
          <h2 >LATEST UPDATES</h2>
          </div>
      
          <div class="bloglist">
              <div class="row">
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                          <div class="featured-img">
                              <img src="images/blog-1.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div       >
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                      <img src="images/blog-2.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                              <img src="images/blog-3.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="get-button">
                  <a href="#" class="btn btn-play">View More</a>
              </div>
          </div>
      </div>
      <div class="topslant">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1320" height="50" viewBox="0 0 1310 1">
              <defs>
                  <clipPath id="clip-path">
                  <path id="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" />
                  </clipPath>
              </defs>
              <g id="footer-top" transform="translate(0 -0.5)">
                  <path id="Mask-2" data-name="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" fill="#0c0f0b"/>
              </g>
          </svg>
      </div>
    </section>
</div><!--end body-->
<?php include_once('footer.php'); ?>
   