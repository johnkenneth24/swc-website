
  <?php include('header-home.php'); ?>
  <div class="banner">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-7 banner-desc">
          <div class="inner">
            <h1>WELCOME TO <br/>BITCOIN POKER</h1>
            <p>ANONYMOUS. LOW RAKE. DAILY TOURNAMENTS.</p>
            <div class="get-button">
              <a href="#" class="btn playnow">Play Now</a>
              <a href="#" class="btn btn-download">Download</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-5 featured-img">
          <img src="images/bitcoin-img.png" alt="">
        </div>
      </div>
    </div>
  </div>
<div class="body">
        <section class="tournament-table">
            <div class="container">
                <h2>UPCOMING BITCOIN POKER TOURNAMENTS</h2>
                <div class="table-tournament table-poker table-responsive">
                    <table> 
                        <tr>
                        <th>Start</th>
                        <th>TOURNAMENT NAME</th>
                        <th>GAME</th>
                        <th>BUY-IN</th>  
                        <th>&nbsp;</th>
                        </tr>
                        <tr>
                            <td width="20%">11 minutes</td>
                            <td>HORSE</td>
                            <td>Mixed FL</td>
                            <td>95 + 5</td>
                            <td><a href="#" class="btn btn-play">PLAY NOW</a></td>
                        </tr>
                        <tr>
                            <td>11 minutes</td>
                            <td>Big O Freeroll [1k uBCH]</td>
                            <td>5 Card Omaha H/L PL</td>
                            <td>Free Roll</td>
                            <td><a href="#" class="btn btn-play">PLAY NOW</a></td>
                        </tr>
                        <tr>
                            <td>41 minutes</td>
                            <td>NLH [20k GTD]</td>
                            <td>Hold'em NL</td>
                            <td>940 + 60</td>
                            <td><a href="#" class="btn btn-play">PLAY NOW</a></td>
                        </tr>
                        <tr>
                            <td>2 hours 11 minutes</td>
                            <td>Tiny Turbo [500 GTD]</td>
                            <td>Hold'em NL</td>
                            <td>47 + 3</td>
                            <td><a href="#" class="btn btn-play">PLAY NOW</a></td>
                        </tr>
                        <tr>
                            <td>2 hours 41 minutes</td>
                            <td>NLH</td>
                            <td>Hold'em NL</td>
                            <td>950 + 50</td>
                            <td><a href="#" class="btn btn-play">PLAY NOW</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </section>

        <section class="section-blog">
            <div class="container">
                <div class="title">
                <h2 >LATEST PROMOTIONS</h2>
                </div>
            
                <div class="bloglist">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="inner">
                                <div class="featured-img">
                                    <img src="images/blog-1.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <span>23 June 2021</span>
                                    <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                                </div       >
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="inner">
                            <div class="featured-img">
                            <img src="images/blog-2.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <span>23 June 2021</span>
                                    <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="inner">
                            <div class="featured-img">
                                    <img src="images/blog-3.jpg" alt="">
                                </div>
                                <div class="desc">
                                    <span>23 June 2021</span>
                                    <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="get-button">
                        <a href="#" class="btn btn-play">View More</a>
                    </div>
                </div>
            </div>
            <div class="topslant">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1320" height="50" viewBox="0 0 1310 1">
                    <defs>
                        <clipPath id="clip-path">
                        <path id="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" fill="#0e180b" style="fill:#0e180b"/>
                        </clipPath>
                    </defs>
                    <g id="footer-top" transform="translate(0 -0.5)">
                        <path id="Mask-2" data-name="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" fill="#0c0f0b"/>
                    </g>
                </svg>
            </div>
        </section>

        <section class="section-faq">
            <div class="container">
                <h2>BITCOIN FREQUENTLY ASKED QUESTIONS</h2>
                <div class="accordion" id="accordionExample">
              <div class="accordion-item">
                  <h2 class="accordion-header" id="headingOne">
                  <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  What is Bitcoin?
                  </button>
                  </h2>
                  <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                  <div class="accordion-body">
                      <p>Bitcoin is an electronic currency and payment system based on an open-source, peer-to-peer internet protocol. There is no centralized issuer; instead individual users volunteer computing power, executing and recording the Bitcoin network’s transactions in a decentralized way. More information can be found at bitcoin.org and blockchain.info.</p>
                  </div>
                  </div>
              </div>
              <div class="accordion-item">
                  <h2 class="accordion-header" id="headingTwo">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  Why do bitcoins have value?
                  </button>
                  </h2>
                  <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                  <div class="accordion-body">
                      <p>Bitcoins have value because people find the Bitcoin protocol to be a useful means of value transfer.  Because there is a fixed amount of bitcoin, the value is purely determined by aggregate supply and demand.  So it’s like a currency, but it isn’t backed by a government?</p>

                      <p>That doesn’t seem right. Bitcoin begs the question: “Can there exist a currency backed purely by mathematics  and computing power, instead of a central issuer?” Government backed fiat currency has long been the norm.  In a fiat system, units of currency can be added or subtracted at the whim of the issuer.</p>

                      <p>Staunch Bitcoin supporters feel it’s no advantage to the health of a currency when a few people have the  ability to make arbitrary changes to it. With fiat currencies, the issuing government is charged with  protecting all aspects of the currency, including counterfeiting.

                      <p>In the case of Bitcoin, math and computing power replaces government. The anti-counterfeiting duties are  performed by computer code, and have never been defeated. There are many other aspects to consider on the  subject of mathematics and distributed power currency governance. The Bitcoin project is truly an economic  experiment.</p>
                  </div>
                  </div>
              </div>
              <div class="accordion-item">
                  <h2 class="accordion-header" id="headingThree">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  Where can I buy and sell bitcoin?
                  </button>
                  </h2>
                  <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                  <div class="accordion-body">
                  <p>The short answer is: you can buy bitcoin from anyone who has it, and sell bitcoin to anyone who wants it.  Bitcoin is permissionless, easy to send and receive in any quantity, and functions similar to any  other currency.</p>

                  <p> There are many centralized exchanges, as well as decentralized options. A growing number of merchants accept  or exchange bitcoin, and bitcoin ATMs can be found around the world. We do not endorse any one  exchange method.</p>

                  <p>This seems so complicated. I just want to play poker.</p>

                  <p>We offer this information for anyone who is interested. Understanding the science behind Bitcoin is  irrelevant to playing on Seals. Once you have an account balance, it’s normal poker from there. Normal  poker with low rake, and cashouts in less than 12 hours, thanks largely to the effectiveness of what we’re  talking about.</p>
                  </div>
                  </div>
              </div>
              </div>
            </div>
       </section>

        <section class="section-ourteam">
            <div class="container">
                <h2>The team behind the game</h2>
                <ul class="team">
                    <li>
                        <div class="team-img">
                            <img src="images/team-1.png" alt="">
                        </div>
                        <h3>John</h3>
                        <span>CEO</span>
                    </li> 
                    <li>
                        <div class="team-img">
                        <img src="images/team-2.png" alt="">
                        </div>
                        <h3>Jacob</h3>
                        <span>CTO</span>
                    </li> 
                    <li>
                        <div class="team-img">
                        <img src="images/team-3.png" alt="">
                        </div>
                        <h3>Anne</h3>
                        <span>CTO</span>
                    </li> 
                    <li>
                        <div class="team-img">
                        <img src="images/team-4.png" alt="">
                        </div>
                        <h3>Ella</h3>
                        <span>CTO</span>
                    </li> 
                    <li>
                        <div class="team-img">
                            <img src="images/team-6.png" alt="">
                        </div>
                        <h3>Allan</h3>
                        <span>CTO</span>
                    </li> 
                    <li>
                        <div class="team-img">
                        <img src="images/team-5.png" alt="">
                        </div>
                        <h3>Lyca</h3>
                        <span>CTO</span>
                    </li> 
                    
                </ul>
            </div>
        </section>
</div><!--end body-->
<?php include_once('footer.php'); ?>
   