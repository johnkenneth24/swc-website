<?php include_once('header.php'); ?>
<div class="body page">
    <section class="content-section">
      <div class="container">
       <h1>HOUSE RULES</h1>
       <div class="row">
           <div class="col-lg-8 col-md-12 index-links">            
                <div class="row ">
                    <div class="col-md-6 item">
                          <h4>SwC Poker FAQ</h4>
                          <div class="links">
                              <ul>
                              <li><a href="#">What is SwC Poker?</a></li>
                              <li><a href="#">What is Bitcoin?</a></li>
                              <li><a href="#">What is a Chip worth?</a></li>
                              <li><a href="#">What are the minimum deposit and withdrawal amounts? </a></li>
                              <li><a href="#">How fast are deposits and withdrawals? </a></li>
                              <li><a href="#">What are the costs of deposits and withdrawals? </a></li>
                              <li><a href="#">Do you require my dox/identification? </a></li>
                              <li><a href="#">Do you accept USA players? Other countries? </a></li>
                              <li><a href="#"> Do you segregate player funds? </a></li>
                            </ul>

                          </div>
                    </div>
                    <div class="col-md-6 item">
                          <div class="links">
                          <ul>
                              <li><a href="#">What is SwC Poker?</a></li>
                              <li><a href="#">What is Bitcoin?</a></li>
                              <li><a href="#">What is a Chip worth?</a></li>
                              <li><a href="#">What are the minimum deposit and withdrawal amounts? </a></li>
                              <li><a href="#">How fast are deposits and withdrawals? </a></li>
                              <li><a href="#">What are the costs of deposits and withdrawals? </a></li>
                              <li><a href="#">Do you require my dox/identification? </a></li>
                              <li><a href="#">Do you accept USA players? Other countries? </a></li>
                              <li><a href="#"> Do you segregate player funds? </a></li>
                            </ul>
                          </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-6 item">
                        <h4>Cardroom Rules</h4>
                          <div class="links">
                              <ul>
                              <li><a href="#">General Rules </a></li>
                              <li><a href="#">Chat Rules </a></li>
                              <li><a href="#">Cash Game Rules </a></li>
                              <li><a href="#">Tournament Rules </a></li>
                              <li><a href="#">Bots are Prohibited </a></li>
                              </ul>
                          </div>
                          <h4>Mixed Poker Games </h4>
                          <div class="links">
                              <ul>
                                <li> <a href="#"> See all Mixed Poker Games  </a></li>      
                              </ul>
                          </div>
                    </div>
                </div>
           </div>
           <div class="col-lg-4 col-md-12 sidenav">
                <div class="widget">
                <h5>CATEGORIES</h5>
                   <ul>
                    <li><a href="#">Updates</a></li>
                    <li><a href="#">Promotions </a></li>
                    <li><a href="#">Blog </a></li>
                    <li><a href="#">Blockchain Poker Announcements </a></li>
                    <li><a href="#">Bitcoin Poker Tournaments </a></li>
                    <li><a href="#">Bitcoin Poker Freerolls </a></li>
                    <li><a href="#">Crypto Poker Promotions </a></li>
                    <li><a href="#">Crypto Poker Cash Games </a></li>
                    <li><a href="#">BTC Poker Bad Beat </a></li>
                    <li><a href="#">Blockchain Poker Strategy </a></li>
                    <li><a href="#">Cryptocurrency Gambling Industry </a></li>
                    <li><a href="#"> Bitcoin Jackpots </a></li>
                  </ul>
                </div>
                <div class="widget">
                    <h5>MORE TO READ</h5>
                   <ul>
                    <li><a href="#"> House Rules & FAQ </a></li>
                    <li><a href="#">Poker Game Rules </a></li>
                    <li><a href="#">Hand Rankings </a></li>
                    <li><a href="#">Game & Betting Styles </a></li>
                    <li><a href="#">Bitcoin FAQ </a></li>
                    <li><a href="#">Promotions </a></li>
                    <li><a href="#">Download </a></li>
                    <li><a href="#">Play Now </a></li>

                  </ul>
                </div>
                <div class="widget">
                    <h5>RECENT BLOG POSTS</h5>
                   <ul>
                    <li><a href="#">More Blogpost</a></li>
                  </ul>
                </div>
                
           </div>
       </div>
    </div>
    </section> 
    <section class="section-blog poker-faq">
      <div class="container">
          <div class="title">
            <h2>FAQ</h2>
          </div>
          <section class="section-faq">
            <div class="container">
                <h2>BITCOIN FREQUENTLY ASKED QUESTIONS</h2>
                <div class="accordion" id="accordion-faq">
              <div class="accordion-item">
                  <h2 class="accordion-header" id="headingOne">
                  <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  Who are you, and what is SwC Poker?
                  </button>
                  </h2>
                  <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordion-faq">
                  <div class="accordion-body">
                      <p>We are a small team of humans dedicated to building a secure, fair, low rake Bitcoin poker platform. We offer an honest, secure Bitcoin poker room. Player balances are denominated in Bitcoin, and all cash-ins and cash-outs are done via Bitcoin. No traditional currency is ever used. The organization chooses to remain anonymous. We’re committed to Bitcoin poker for the long haul.</p>
                  </div>
                  </div>
              </div>
              <div class="accordion-item">
                  <h2 class="accordion-header" id="headingTwo">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  What is Bitcoin?
                  </button>
                  </h2>
                  <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordion-faq">
                  <div class="accordion-body">
                      <p>Bitcoin is a popular peer-to-peer digital crypto-currency that has been in use since 2009. SwC Poker only transacts business in Bitcoin.  We make depositing and withdrawing Bitcoin fast and easy. (More information is available on our Bitcoin FAQ page).</p>
                  </div>
                  </div>
              </div>
              <div class="accordion-item">
                  <h2 class="accordion-header" id="headingThree">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  What Is a chip worth?
                  </button>
                  </h2>
                  <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordion-faq">
                  <div class="accordion-body">
                  <p>1 Chip on SwC Poker is worth 0.000001 BTC. This unit is also known as 1 bit, or 1 microBTC (μBTC). 1,000,000 chips is equal to 1 Bitcoin.</p>
                  </div>
                  </div>
                  </div>
              </div>
              <div class="accordion-item">
                  <h2 class="accordion-header" id="headingFour">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                  What are the minimum deposit & withdrawal amounts?
                  </button>
                  </h2>
                  <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordion-faq">
                  <div class="accordion-body">
                      <p>There is no minimum deposit. All deposits are credited, down to the smallest unit of Bitcoin, 1 satoshi, also expressed as 0.00000001 BTC or 0.01 SwC Poker chips. The minimum withdrawal amount is 500 chips.</p>
                  </div>
                  </div>
              </div>
              <div class="accordion-item"> 
                  <h2 class="accordion-header" id="headingFive">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                  How fast are deposits and withdrawals?‍
                  </button>
                  </h2>
                  <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordion-faq">
                  <div class="accordion-body">
                      <p>Deposits are credited after just 1 confirmation on the Bitcoin network. The time for this is variable, but averages 10-30 minutes. Withdrawals are processed regularly, averaging 4-6 hours.  On rare occasion, withdrawals may take up to 24 hours.‍</p>
                  </div>
                  </div>
              </div>
              <div class="accordion-item"> 
                  <h2 class="accordion-header" id="headingSix">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                  What are the costs of deposits & withdrawals?
                  </button>
                  </h2>
                  <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordion-faq">
                  <div class="accordion-body">
                 <p> A transaction fee must be paid to the Bitcoin network for all transfers of bitcoin. The fee required for a fast transaction is normally small, but varies based on network demand.</p>

                <p>Depositing and withdrawing Bitcoin requires paying this network fee. For withdrawals, the required network fee is deducted from the total amount withdrawn. Players pay only network fees and SwC Poker does not charge any additional fee.</p>
                  </div>
                  </div>
              </div>
              <div class="accordion-item"> 
                  <h2 class="accordion-header" id="headingSeven">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                  Do you require my dox/identification?‍
                  </button>
                  </h2>
                  <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven" data-bs-parent="#accordion-faq">
                  <div class="accordion-body">
                 <p> No. We at SwC Poker flatly reject the idea that an online poker room should require your personal information. We believe the online poker experience should be similar to a live cardroom, whereby players can buy chips, play, and cash out anonymously. By using Bitcoin, SwC Poker can securely, anonymously and quickly process transactions without requiring any documentation. Only an e-mail address is required to create and use an account.‍</p>
                  </div>
                  </div>
              </div>
              <div class="accordion-item"> 
                  <h2 class="accordion-header" id="headingEight">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                  Do you accept USA players? Other countries?‍
                  </button>
                  </h2>
                  <div id="collapseEight" class="accordion-collapse collapse" aria-labelledby="headingEight" data-bs-parent="#accordion-faq">
                  <div class="accordion-body">
                 <p> SwC Poker gladly accepts players worldwide.‍‍</p>
                  </div>
                  </div>
              </div>
              <div class="accordion-item"> 
                  <h2 class="accordion-header" id="headingEight-1">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEight-1" aria-expanded="false" aria-controls="collapseEight-1">
                  Do you have a player point system like other poker sites?‍
                  </button>
                  </h2>
                  <div id="collapseEight-1" class="accordion-collapse collapse" aria-labelledby="headingEight" data-bs-parent="#accordion-faq">
                  <div class="accordion-body">
                 <p> Yes. We have a point system called “Krill.” The details can be found on our Krill Rewards Program page.‍‍‍</p>
                  </div>
                  </div>
              </div>
              <div class="accordion-item"> 
                  <h2 class="accordion-header" id="headingNine">
                  <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                  How much do you charge in rake?‍
                  </button>
                  </h2>
                  <div id="collapseNine" class="accordion-collapse collapse" aria-labelledby="headingNine" data-bs-parent="#accordion-faq">
                  <div class="accordion-body">
                    <p> All units shown are Chips. 1 Chip on SwC Poker is worth 0.000001 BTC. This unit is also known as 1 bit, or 1 microBTC (μBTC).No rake is collected in flop game hands that do not make it to the flop. This also applies to draw games (no first draw, no rake) and stud games (no fourth street, no rake).‍‍‍</p>
                    <h3>Fixed Limit Games</h3>
                    <div class="table-tournament table-poker table-responsive">
                        <table> 
                            <tr>
                            <th width="30%">Stakes</th>
                            <th>Rake (%)</th>
                            <th>Max Rake</th>
                            <th>&nbsp;</th>  
                            <th>&nbsp;</th>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2"></td>
                                <td>2 Players</td>
                                <td>3 Players</td>
                                <td>4+ Players</td>
                                
                            </tr>
                            <tr>
                                <td>Up to 5/10</td>
                                <td>4.00%</td>
                                <td>1</td>
                                <td>2</td>
                                <td>2</td>
                            </tr>
                            <tr>
                                <td>10/20</td>
                                <td>3.00%</td>
                                <td>6</td>
                                <td>8</td>
                                <td>8</td>
                            </tr>
                            <tr>
                                <td>25/50 to 50/100</td>
                                <td>3.00%</td>
                                <td>10</td>
                                <td>14</td>
                                <td>20</td>
                            </tr>
                            <tr>
                                <td>100/200 to 200/400</td>
                                <td>2.50%</td>
                                <td>10</td>
                                <td>40</td>
                                <td>60</td>
                            </tr>
                            <tr>
                                <td>300/600 to 500/1000</td>
                                <td>2.00%</td>
                                <td>20</td>
                                <td>40</td>
                                <td>60</td>
                            </tr>
                            <tr>
                                <td>000/2000 and up</td>
                                <td>1.00%	</td>
                                <td>20</td>
                                <td>40</td>
                                <td>60</td>
                            </tr>
                        </table>
                    </div>
                    <h3>No Limit & Pot Limit Games</h3>
                    <div class="table-tournament table-poker table-responsive">
                        <table> 
                            <tr>
                            <th width="30%">Stakes</th>
                            <th>Rake (%)</th>
                            <th>Max Rake</th>
                            <th>&nbsp;</th>  
                            <th>&nbsp;</th>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2"></td>
                                <td>2 Players</td>
                                <td>3 Players</td>
                                <td>4+ Players</td>
                                
                            </tr>
                            <tr>
                                <td>Up to 0.10/0.20</td>
                                <td>0.00%</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>1/2 to 5/10</td>
                                <td>4.00%</td>
                                <td>10</td>
                                <td>20</td>
                                <td>45</td>
                            </tr>
                            <tr>
                                <td>10/20</td>
                                <td>4.00%</td>
                                <td>19</td>
                                <td>25</td>
                                <td>60</td>
                            </tr>
                            <tr>
                                <td>25/50 and u</td>
                                <td>3.60%</td>
                                <td>27</td>
                                <td>35</td>
                                <td>76</td>
                            </tr>
                        
                        </table>
                    </div>
                    <h3>No Limit Bad Beat Jackpot Games</h3>
                    <p>The rake is 25% higher in qualifying Bad Beat Jackpot hands with 4 or more players. This extra portion of rake is entirely contributed to the jackpot.</p>

                    <p>2- and 3- handed games at Bad Beat Jackpot tables do not qualify for the jackpot, and do not rake higher than the normal rate.</p>
                    <div class="table-tournament table-poker table-responsive">
                        <table> 
                            <tr>
                            <th width="20%">Stakes</th>
                            <th>Rake (%)</th>
                            <th>Max Rake</th>
                            <th>&nbsp;</th>  
                            <th>Rake (%)</th>
                            <th>Max Rake</th>
                            </tr>
                            <tr>
                                <td width="20%" colspan="2"></td>
                                <td>2 Players</td>
                                <td>3 Players</td>
                                <td>&nbsp;</td>
                                <td>4 Players</td>
                                
                            </tr>
                            <tr>
                                <td>Up to 10/20</td>
                                <td>4.00%</td>
                                <td>19</td>
                                <td>25</td>
                                <td>5.00%</td>
                                <td>75</td>  
                            </tr>
                            <tr>
                                <td>25/50 and up</td>
                                <td>3.60%</td>
                                <td>27</td>
                                <td>35</td>
                                <td>4.50%</td>
                                <td>95</td>  
                            </tr>
                        
                        </table>
                    </div>
                    <h3>No Limit & Pot Limit Heads-Up Tables</h3>
                    <div class="table-tournament table-poker table-responsive column-table">
                        <table> 
                            <tr>
                            <th width="30%">Stakes</th>
                            <th width="30%">Rake (%)</th>
                            <th width="30%">Max Rake</th>
                            </tr>
                            <tr>
                                <td>Up to to 10/20</td>
                                <td>3.00%</td>
                                <td>21</td>
                            </tr>
                            <tr>
                                <td>25/50 to 100/200</td>
                                <td>3.00%</td>
                                <td>35</td>
                            </tr>
                            <tr>
                                <td>200/400 and up</td>
                                <td>3.00%</td>
                                <td>55</td>
                            </tr>
                           
                        
                        </table>
                    </div>
                    <h3>Chinese Poker</h3>
                    <p>Chinese Poker is played in rounds of multiple hands. Individual hands are not raked. At the end of each complete round, the player or players who won are charged rake on their net winnings in that round, according to the table below.</p>
                    <div class="table-tournament table-poker table-responsive column-table">
                        <table> 
                            <tr>
                            <th width="30%">Stakes</th>
                            <th width="30%">Rake (%)</th>
                            <th width="30%">Max Rake</th>
                            </tr>
                            <tr>
                                <td>Up to 10/point</td>
                                <td>3.00%</td>
                                <td>50</td>
                            </tr>
                            <tr>
                                <td>25/point</td>
                                <td>3.00%</td>
                                <td>100</td>
                            </tr>
                            <tr>
                                <td>50/point to 100/point</td>
                                <td>3.00%</td>
                                <td>150</td>
                            </tr>
                            <tr>
                                <td>200/point and up</td>
                                <td>3.00%</td>
                                <td>200</td>
                            </tr>
                           
                        
                        </table>
                    </div>
                    <p>Our rakeback system, rewards and promotions make the rake on SwC Poker among the lowest in the world. The rake at our cash tables is periodically reviewed and adjusted to remain in line with the value of Bitcoin. An <a href="#">archive of past rake schedules</a> is maintained for reference.</p>
                    <h3>Tournaments</h3>
                    <div class="table-tournament table-poker table-responsive column-table">
                        <table> 
                            <tr>
                            <th width="50%">Type</th>
                            <th width="50%">Entry Fee</th>
                            </tr>
                            <tr>
                                <td>Sit & Gos</td>
                                <td>2% or less</td>
                            </tr>
                            <tr>
                                <td>Scheduled tournaments</td>
                                <td>5% or less</td>
                            </tr>
                            <tr>
                                <td>Guaranteed prize pool tournaments</td>
                                <td>6% or less</td>
                            </tr>
                        
                        </table>
                    </div>
                    <p>Tournaments with a total buy-in lower than 1 chip have no entry fee.</p>
                    <h2>How do I know my opponents aren’t cheating?‍</h2>
                    <p>SwC Poker monitors and vigilantly fights any collusion, cheating, or otherwise unfair gameplay. Additionally, you may email support@swcpoker.club if you observe anything you would like us to investigate. We reserve the right to handle cheating, collusion, or otherwise unfair play at our sole discretion. If you are uncomfortable with anonymous ring games, we offer heads-up tables.‍</p>
                    <h2>Cardroom Rules</h2>
                    <h3>General Rules‍</h3>
                    <ul>
                        <li>Sale or transfer of accounts between players is prohibited.</li>
                        <li>Accounts may not change ownership. Purchasing or receiving an account from another player will result in its confiscation.‍</li>
                        <li>Usernames may not contain vulgar terms or racial epithets.</li>
                        <li>Player-to-player chip transfers are at your own risk. SwC Poker management does not intervene in player disputes related to chip transfers. Chip transfers cannot be cancelled or reversed. Please exercise caution and good judgement before performing any player-to-player transfers in an anonymous poker room.‍</li>
                        <li>Multiple players in the same home are welcome to play at SwC Poker, but not at the same cash tables, tournaments, or sit &amp; gos.</li>
                    </ul>
                    <h3>Chat Rules‍</h3>
                    <ul>
                        <li>Chat is a privilege that can be revoked by management at will.‍ Verbally abusing players will not be tolerated.</li>
                        <li>Chat is uncensored, and is not necessarily in English.‍</li>
                        <li>Players are provided with an Ignore option to hide the chat of any player they do not wish to see.‍</li>
                    </ul>
                    <ol class="alpha">
                        <li> Coarse language is permitted, but racial/sexual slurs and hate speech are prohibited.</li>
                        <li>Discussing a hand in progress, revealing your hole cards prematurely, or otherwise affecting gameplay is prohibited.‍</li>
                        <li>Spamming or flooding the chat is prohibited.‍</li>
                        <li>Illegal, malicious, and sexually explicit hyperlinks or chat is prohibited.‍</li>
                        <li>Referral codes, referral hyperlinks, and advertisements for external promotions or sites are prohibited.</li>
                        <li>Begging for chips at the table or in the lobby chat box is prohibited</li>
                    </ol>
                    <ul>
                        <li>In addition to the rules listed above, a common sense policy should be used to determine what is considered inappropriate chat. Cases will be dealt with by management on an individual basis.</li>
                        <li>Players found to be using inappropriate chat will be given a warning, and will have their chat removed for one week.‍</li>
                        <li>A second violation will result in the removal of chat for one month.‍</li>
                        <li>Gross and/or repeated violations of prohibited behavior will result in a permanent loss of chat.</li>
                        <li>Do not use multiple accounts to evade a loss of chat, or to chat to a user who has ignored you. This may result in escalated consequences at the discretion of SwC management.‍</li>
                        <li>In general, 1st infraction = 1 week chat loss, 2nd infraction = 1 month chat loss, 3rd infraction = permanent chat loss.‍</li>
                        <li>Doxxing another player will result in permanent loss of chat.</li>
                    </ul>
                    <h3>Cash Game Rules‍‍</h3>
                    <ul>
                        <li>One player to a hand.</li>
                        <li>Multiple players in the same home are welcome to play at SwC Poker, but not at the same cash tables.</li>
                        <li>Fixed Limit games have a cap of 4 bets per street when the action is multi-way, and no cap when the action is heads up.</li>
                        <li>All games have a 1 hour “rathole timer”. Players returning to a table less than 1 hour after leaving it must sit with at least the amount they left with. Using a second account to evade this restriction is prohibited.</li>
                        <li>In mixed games, players are expected to play by the spirit of the game, sitting in during all game types. It is acceptable to occasionally miss a hand or a round, but it is not okay to always skip a particular game type or several game types (for example, sitting out all the stud games, or sitting out the big bet games). Management reserves the right to make decisions and potentially take punitive action in these cases.‍</li></ul>
                        <h3>Tournament Rules‍</h3>
                        <ul>
                        <li>One player to a hand.</li>
                        <li>Multiple players in the same home are welcome to play at SwC Poker, but not in the same tournaments or sit & gos.‍</li>
                        <li>You may unregister at any time before the start of a tournament and receive your buy-in back.‍</li>
                        <li>If a tournament is interrupted and cannot be completed, management will return buy-ins or distribute the prize pool at its discretion.‍</li>
                        <li>One account per person per tournament. Re-entering a tournament on a different account is prohibited.‍</li></ul>
                        <h3>Bots are Prohibited‍</h3>
                        <p>Bots are prohibited on SwC Poker. Please contact <a href="mailto:support@swcpoker.club" title="Contact SwC Poker">support@swcpoker.club</a> if you feel a bot is playing. Management will vigilantly defend the integrity of the games.‍</p>
                  </div>
                  </div>
              </div>
            </div>
       </section>
         
      </div>
      <div class="topslant">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1320" height="50" viewBox="0 0 1310 1">
              <defs>
                  <clipPath id="clip-path">
                  <path id="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" />
                  </clipPath>
              </defs>
              <g id="footer-top" transform="translate(0 -0.5)">
                  <path id="Mask-2" data-name="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" fill="#0c0f0b"/>
              </g>
          </svg>
      </div>
    </section> 
    <section class="section-blog latest-promition">
      <div class="container">
          <div class="title">
          <h2 >LATEST PROMOTIONS</h2>
          </div>
      
          <div class="bloglist">
              <div class="row">
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                          <div class="featured-img">
                              <img src="images/blog-1.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div       >
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                      <img src="images/blog-2.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                              <img src="images/blog-3.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="get-button">
                  <a href="#" class="btn btn-play">View More</a>
              </div>
          </div>
      </div>
      <div class="topslant">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1320" height="50" viewBox="0 0 1310 1">
              <defs>
                  <clipPath id="clip-path">
                  <path id="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" />
                  </clipPath>
              </defs>
              <g id="footer-top" transform="translate(0 -0.5)">
                  <path id="Mask-2" data-name="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" fill="#0c0f0b"/>
              </g>
          </svg>
      </div>
    </section>
</div><!--end body-->
<?php include_once('footer.php'); ?>
   