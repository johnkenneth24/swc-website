<?php include_once('header.php'); ?>
<div class="body page">
    <section class="content-section">
      <div class="container">
       <h1>Promotions</h1>

     </div>
    </section> 

    <section class="section-blog latest-promition">
      <div class="container">
          <div class="title">
          <h2 >LATEST PROMOTIONS</h2>
          </div>
      
          <div class="bloglist">
              <div class="row">
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                          <div class="featured-img">
                              <img src="images/blog-1.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div       >
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                      <img src="images/blog-2.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                              <img src="images/blog-3.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                          <div class="featured-img">
                              <img src="images/blog-1.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div       >
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                      <img src="images/blog-2.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                              <img src="images/blog-3.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="get-button">
                  <a href="#" class="btn btn-play">View More</a>
              </div>
          </div>
      </div>
      <div class="topslant">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1320" height="50" viewBox="0 0 1310 1">
              <defs>
                  <clipPath id="clip-path">
                  <path id="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" />
                  </clipPath>
              </defs>
              <g id="footer-top" transform="translate(0 -0.5)">
                  <path id="Mask-2" data-name="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" fill="#0c0f0b"/>
              </g>
          </svg>
      </div>
    </section>
</div><!--end body-->
<?php include_once('footer.php'); ?>
   