<?php include_once('header.php'); ?>
<div class="body page">
    <section class="content-section">
      <div class="container d-flex justify-content-center"> 
        <div class="sideshift-order"> 

            <div class="order-head d-flex justify-content-between flex-wrap"> 
                <div class="exchange-header  d-flex align-items-center">
                    <img class="token-icon" src="images/cryptocoin/bch-logo.png" alt=""> 
                    <p class="token-name">BCH</p>
                    <i class="pe-2 fa-solid fa-right-long"></i>
                    <img class="token-icon" src="images/cryptocoin/btc.eecdd087.svg" alt="">
                    <p class="token-name">BTC</p>
                </div>

                <div class="order-id">
                    <p class="order text-end mb-0">ORDER</p>
                    <p class="order-no">50b0537d9e73a461fdd1</p>
                </div>
            </div> 

            <div class="waiting-send">
                <h2 class="mb-0 text-light text-center">WAITING FOR YOU TO SEND BCH ...</h2>
                <p class="pt-2 text-center">1 BCH = 0.00767268 BTC <span title="#"><i class="fa-regular fa-circle-question"></i></span></p>
            </div>

            <div class="row mt-5 mb-5">
                <div class="col-md-6 col-sm-12">
                    <h3 class="pb-3 send">PLEASE SEND</h3>
                    <div class="min d-flex justify-content-between align-items-center">
                        <h6 class="rate">Min <span title="#"><i class="fa-regular fa-circle-question"></i></span></h6>
                        <p class="rate-value">0.02710408 BCH</p>
                    </div>
                    <div class="min d-flex justify-content-between align-items-center">
                        <h6 class="rate">Max <span title="#"><i class="fa-regular fa-circle-question"></i></span></h6>
                        <p class="rate-value">108.41633885 BCH</p>
                    </div>
                    <div class="mt-3 address">
                        <h3 class="send">TO ADDRESS</h3>
                            <div class="address-input d-flex flex-column justify-content-center">
                                <input class="address-link" type="readonly">
                                <a class="text-center" href="#"><i class="fa-solid fa-clone"></i> Copy address</a>
                            </div>  
                    </div> 
                </div>
                <div class="col-md-6 col-sm-12 d-flex justify-content-center align-items-center">
                    <div class="qr-code">
                        <img src="images/qrcode.png" alt="">
                    </div> 
                </div>
            </div>

            <h2 class="text-center mb-2 fee">ESTIMATED NETWORK FEES: 0.7 USD <i class="fa-regular fa-circle-question"></i></h2>
            <div class="form-check d-flex justify-content-center">
                <input class="form-check-input me-2 fee-box" type="checkbox" value="" id="flexCheckDefault">
                <label class="form-check-label text-start text-nowrap fee" for="flexCheckDefault"> Play sound on shift completion</label>
            </div>

            <div class="mt-5 d-flex justify-content-between flex-wrap address-date">
                <div class="receive-address">
                    <h6 class="order">RECEIVING ADDRESS</h6>
                    <p class="order-no">3LELsMXAT...X47GwQuYU</p>
                </div>
                <div class="date-order">
                    <h6 class="order text-end">CREATED AT</h6>
                    <p class="order-no">2022-03-30 11:39</p>
                </div>
            </div>
            
        </div>
       </div>
    </section>  
    <section class="section-blog latest-promition">
      <div class="container">
          <div class="title">
          <h2 >LATEST UPDATES</h2>
          </div>
      
          <div class="bloglist">
              <div class="row">
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                          <div class="featured-img">
                              <img src="images/blog-1.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div       >
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                      <img src="images/blog-2.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                              <img src="images/blog-3.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="get-button">
                  <a href="#" class="btn btn-play">View More</a>
              </div>
          </div>
      </div>
      <div class="topslant">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1320" height="50" viewBox="0 0 1310 1">
              <defs>
                  <clipPath id="clip-path">
                  <path id="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" />
                  </clipPath>
              </defs>
              <g id="footer-top" transform="translate(0 -0.5)">
                  <path id="Mask-2" data-name="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" fill="#0c0f0b"/>
              </g>
          </svg>
      </div>
    </section>
</div><!--end body-->
<?php include_once('footer.php'); ?>
   