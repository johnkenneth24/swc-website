<?php include_once('header.php'); ?>
<div class="body page">
    <section class="content-section">
      <div class="container">

       <div class="sideshift-popup">
       <h2>Send</h2>
        <div class="search-crypto">
            <input type="text" class="text search-text" placeholder="Search">
        </div><!--end search-cypto-->
        <div class="crypto-list">
            <h2>Popular</h2>
            <ul>
                <li class="active">
                    <img src="images/ada.d11358fa.svg" alt="" class="crypto-icon">
                    <h3>ADA</h3>
                    <p>ADA</p>
                </li>
                <li>
                    <img src="images/avax.d356dd33.svg" alt="" class="crypto-icon">
                    <h3>AVAX</h3>
                    <p>Avalanche</p>
                </li>
                <li>
                    <img src="images/bnb.svg" alt="" class="crypto-icon">
                    <h3>BNB</h3>
                    <p>Binance Coin</p>
                    <span>BSC</span>
                </li>
                <li>
                    <img src="images/cryptocoin/btc.eecdd087.svg" alt="" class="crypto-icon">
                    <h3>BTC</h3>
                    <p>Bitcoin</p> 
                </li>
                <li>
                    <img src="images/bch.1bf7c884.svg" alt="" class="crypto-icon">
                    <h3>BCH</h3>
                    <p>Bitcoin Cash</p>
                </li>
                <li>
                    <img src="images/eth.9270fc02.svg" alt="" class="crypto-icon">
                    <h3>ETH</h3>
                    <p>Etherium</p>
                </li>
                <li>
                    <img src="images/ftm.ccdf69ac.svg" alt="" class="crypto-icon">
                    <h3>FTM</h3>
                    <p>Fantom</p>
                </li>
                <li>
                    <img src="images/ltc.838ffb04.svg" alt="" class="crypto-icon">
                    <h3>LTC</h3>
                    <p>Litecoin</p>
                </li>
                <li>
                    <img src="images/polygon.c894daac.svg" alt="" class="crypto-icon">
                    <h3>Matic</h3>
                    <p>Matic</p>
                </li>
                <li>
                    <img src="images/xmr.7b4d3191.svg" alt="" class="crypto-icon">
                    <h3>XMR</h3>
                    <p>Monero</p>
                </li>
                <li>
                    <img src="images/cryptocoin/xai.svg" alt="" class="crypto-icon">
                    <h3>XAI</h3>
                    <p>SideShift Token</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/xai.svg" alt="" class="crypto-icon">
                    <h3>XAI</h3>
                    <p>SideShift Token</p>
                    <span>BALANCE</span>
                </li>
                <li>
                    <img src="images/sol.607f41b1.svg" alt="" class="crypto-icon">
                    <h3>Sol</h3>
                    <p>Solana</p> 
                </li>
                <li>
                    <img src="images/usdt.004b5e55.svg" alt="" class="crypto-icon">
                    <h3>USDT</h3>
                    <p>Tether</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/usdc-logo.png" alt="" class="crypto-icon">
                    <h3>USDC</h3>
                    <p>USDC</p>
                    <span>ERC-20</span>
                </li>
            </ul>
        </div>
        <div class="crypto-list">
            <h2>DEFI</h2>
            <ul>
                <li class="active">
                    <img src="images/cryptocoin/axs-logo.svg" alt="" class="crypto-icon">
                    <h3>AXS</h3>
                    <p>Axie Infinity</p>
                    <span>ERC-20</span> 
                </li>
                <li>
                    <img src="images/cryptocoin/bch-logo.png" alt="" class="crypto-icon">
                    <h3>BCH</h3>
                    <p>Bitcoin Cash</p>
                    <span>SMARTBCH</span>
                </li>
                <li>
                    <img src="images/cryptocoin/comp-logo.svg" alt="" class="crypto-icon">
                    <h3>COMP</h3>
                    <p>Compound</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/eth-logo.svg" alt="" class="crypto-icon">
                    <h3>ETH</h3>
                    <p>Ethereum</p>
                    <span>OPTIMISM</span>
                </li>
                <li>
                    <img src="images/cryptocoin/eth-logo.svg" alt="" class="crypto-icon">
                    <h3>ETH</h3>
                    <p>Ethereum</p>
                    <span>ARBITRUM</span>
                </li>
                <li>
                    <img src="images/cryptocoin/ftm-logo.png" alt="" class="crypto-icon">
                    <h3>FTM</h3>
                    <p>Fantom</p>
                    <span>FANTOM</span>
                </li>
                <li>
                    <img src="images/cryptocoin/link-logo.svg" alt="" class="crypto-icon">
                    <h3>LINK</h3>
                    <p>lINK</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/matic-logo.png" alt="" class="crypto-icon">
                    <h3>MATIC</h3>
                    <p>Matic</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/polygon.c894daac.svg" alt="" class="crypto-icon">
                    <h3>Matic</h3>
                    <p>Matic</p>
                    <span>POLYGON</span>
                </li>
                <li>
                    <img src="images/cryptocoin/srm-logo.svg" alt="" class="crypto-icon">
                    <h3>SRM</h3>
                    <p>Serum</p>
                    <span>ERC-20</span>
                </li>
                
                <li>
                    <img src="images/cryptocoin/slp-logo.svg" alt="" class="crypto-icon">
                    <h3>SLP</h3>
                    <p>Smooth Love Potion</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/sol-logo.svg" alt="" class="crypto-icon">
                    <h3>SOL</h3>
                    <p>Solana</p> 
                </li>
                <li>
                    <img src="images/cryptocoin/sushi-logo.svg" alt="" class="crypto-icon">
                    <h3>SUSHI</h3>
                    <p>Sushi</p>
                    <span>ERC-20</span>
                </li> 
                <li>
                    <img src="images/cryptocoin/uni-logo.svg" alt="" class="crypto-icon">
                    <h3>UNI</h3>
                    <p>Uniswap</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/wbtc-logo.svg" alt="" class="crypto-icon">
                    <h3>WBTC</h3>
                    <p>WBTC</p> 
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/flexusd-logo.svg" alt="" class="crypto-icon">
                    <h3>FLEXUSD</h3>
                    <p>flexUSD</p>
                    <span>SMARTBCH</span>
                </li> 
                <li>
                    <img src="images/cryptocoin/flexusd-logo.svg" alt="" class="crypto-icon">
                    <h3>FLEXUSD</h3>
                    <p>flexUSD</p>
                    <span>SLP</span>
                </li>
                <li>
                    <img src="images/cryptocoin/yfi-logo.svg" alt="" class="crypto-icon">
                    <h3>YFI</h3>
                    <p>yearn.finance</p> 
                    <span>ERC-20</span>
                </li>
            </ul>
        </div>
        <div class="crypto-list">
            <h2>STABLECOIN</h2>
            <ul>
                <li class="active">
                    <img src="images/cryptocoin/busd-logo.svg" alt="" class="crypto-icon">
                    <h3>BUSD</h3>
                    <p>BUSD</p>
                    <span>BEP-20</span> 
                </li>
                <li>
                    <img src="images/cryptocoin/dai-logo.svg" alt="" class="crypto-icon">
                    <h3>DAI</h3>
                    <p>Dai</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/usdh-logo.svg" alt="" class="crypto-icon">
                    <h3>USDH</h3>
                    <p>HonestCoin</p>
                    <span>SLP</span>
                </li>
                <li>
                    <img src="images/cryptocoin/mim-logo.svg" alt="" class="crypto-icon">
                    <h3>MIM</h3>
                    <p>Magic Internet Money</p>
                    <span>AVALANCHE</span>
                </li>
                <li>
                    <img src="images/cryptocoin/ust-logo.svg" alt="" class="crypto-icon">
                    <h3>UST</h3>
                    <p>TerraUSD</p>
                    <span>TERRA</span>
                </li>
                <li>
                    <img src="images/cryptocoin/usdt-logo.svg" alt="" class="crypto-icon">
                    <h3>USDT</h3>
                    <p>Tether</p>
                    <span>SOLANA</span>
                </li>
                <li>
                    <img src="images/cryptocoin/usdt-logo.svg" alt="" class="crypto-icon">
                    <h3>USDT</h3>
                    <p>Tether</p>
                    <span>LIQUID</span>
                </li>
                <li>
                    <img src="images/cryptocoin/usdt-logo.svg" alt="" class="crypto-icon">
                    <h3>USDT</h3>
                    <p>Tether</p>
                    <span>SLP</span>
                </li>
                <li>
                    <img src="images/cryptocoin/usdt-logo.svg" alt="" class="crypto-icon">
                    <h3>USDT</h3>
                    <p>Tether</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/usdt-logo.svg" alt="" class="crypto-icon">
                    <h3>USDT.E</h3>
                    <p>Tether <br> (USDT.e)</p>
                    <span>AVALANCHE</span>
                </li>
                
                <li>
                    <img src="images/cryptocoin/xaut.svg" alt="" class="crypto-icon">
                    <h3>XAUT</h3>
                    <p>Tether Gold</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/usdc-logo.png" alt="" class="crypto-icon">
                    <h3>USDC</h3>
                    <p>USDC</p> 
                    <span>SOLANA</span>
                </li>
                <li>
                    <img src="images/cryptocoin/usdc-logo.png" alt="" class="crypto-icon">
                    <h3>USDC</h3>
                    <p>USDC</p> 
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/usdc-logo.png" alt="" class="crypto-icon">
                    <h3>USDC</h3>
                    <p>USDC</p> 
                    <span>BEP-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/usdc-logo.png" alt="" class="crypto-icon">
                    <h3>USDC.E</h3>
                    <p>USDC.e</p> 
                    <span>AVALANCHE</span>
                </li>
                <li>
                    <img src="images/cryptocoin/flexusd-logo.svg" alt="" class="crypto-icon">
                    <h3>FLEXUSD</h3>
                    <p>flexUSD</p>
                    <span>SMARTBCH</span>
                </li> 
                <li>
                    <img src="images/cryptocoin/flexusd-logo.svg" alt="" class="crypto-icon">
                    <h3>FLEXUSD</h3>
                    <p>flexUSD</p>
                    <span>SLP</span>
                </li> 
            </ul>
        </div>
        <div class="crypto-list">
            <h2>OTHER</h2>
            <ul>
                <li class="active">
                    <img src="images/cryptocoin/bat-logo.svg" alt="" class="crypto-icon">
                    <h3>BAT</h3>
                    <p>Basic Attention Token</p>
                    <span>ERC-20</span> 
                </li>
                <li>
                    <img src="images/cryptocoin/btc.eecdd087.svg" alt="" class="crypto-icon">
                    <h3>BTC</h3>
                    <p>Bitcoin</p>
                    <span>LIGHTNING</span>
                </li>
                <li>
                    <img src="images/cryptocoin/btc-liquid-logo.svg" alt="" class="crypto-icon">
                    <h3>BTC</h3>
                    <p>Bitcoin</p>
                    <span>LIQUID</span>
                </li>
                <li>
                    <img src="images/cryptocoin/dash-logo.svg" alt="" class="crypto-icon">
                    <h3>DASH</h3>
                    <p>Dash</p> 
                </li>
                <li>
                    <img src="images/cryptocoin/dodge-logo.svg" alt="" class="crypto-icon">
                    <h3>DODGE</h3>
                    <p>Dodgecoin</p> 
                </li>
                <li>
                    <img src="images/cryptocoin/ftt-logo.svg" alt="" class="crypto-icon">
                    <h3>FTT</h3>
                    <p>FTX Token</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/shib-logo.svg" alt="" class="crypto-icon">
                    <h3>SHIB</h3>
                    <p>SHIBA INU</p>
                    <span>ERC-20</span>
                </li>
                <li>
                    <img src="images/cryptocoin/xlm-logo.svg" alt="" class="crypto-icon">
                    <h3>XLM</h3>
                    <p>Stellar Lumens</p> 
                </li>
                <li>
                    <img src="images/cryptocoin/luna-logo.svg" alt="" class="crypto-icon">
                    <h3>LUNA</h3>
                    <p>Terra</p>
                    <span>TERRA</span>
                </li>
                <li>
                    <img src="images/cryptocoin/xrp-logo.svg" alt="" class="crypto-icon">
                    <h3>XRP</h3>
                    <p>XRP</p> 
                </li> 
                <li>
                    <img src="images/cryptocoin/zec-logo.svg" alt="" class="crypto-icon">
                    <h3>ZEC</h3>
                    <p>Zcash</p>
                    <span>TRANSPARENT</span>
                </li> 
                <li>
                    <img src="images/cryptocoin/zec-logo.svg" alt="" class="crypto-icon">
                    <h3>ZEC</h3>
                    <p>Zcash</p>
                    <span>SHIELDED</span>
                </li> 
            </ul>
        </div>
        <div class="crypto-list">
            <h2>SIDESHIFT.AI</h2>
            <ul>
                <li class="active">
                    <img src="images/cryptocoin/xai.svg" alt="" class="crypto-icon">
                    <h3>XAI</h3>
                    <p>SideShift Token</p>
                    <span>ERC-20</span> 
                </li>
                <li>
                    <img src="images/cryptocoin/xai.svg" alt="" class="crypto-icon">
                    <h3>XAI</h3>
                    <p>SideShift Token</p>
                    <span>BALANCE</span>
                </li> 
            </ul>
        </div>
       </div>
    </div>
    </section>  
    <section class="section-blog latest-promition">
      <div class="container">
          <div class="title">
          <h2 >LATEST UPDATES</h2>
          </div>
      
          <div class="bloglist">
              <div class="row">
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                          <div class="featured-img">
                              <img src="images/blog-1.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div       >
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                      <img src="images/blog-2.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12">
                      <div class="inner">
                      <div class="featured-img">
                              <img src="images/blog-3.jpg" alt="">
                          </div>
                          <div class="desc">
                              <span>23 June 2021</span>
                              <h2><a href="#">BAD BEAT JACKPOT NOW OVER 1.3 BTC</a></h2>
                              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="get-button">
                  <a href="#" class="btn btn-play">View More</a>
              </div>
          </div>
      </div>
      <div class="topslant">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1320" height="50" viewBox="0 0 1310 1">
              <defs>
                  <clipPath id="clip-path">
                  <path id="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" />
                  </clipPath>
              </defs>
              <g id="footer-top" transform="translate(0 -0.5)">
                  <path id="Mask-2" data-name="Mask" d="M0,48.5H648.553L676,0h566l27.447,48.5H1920v666H0Z" transform="translate(0 0.5)" fill="#0c0f0b"/>
              </g>
          </svg>
      </div>
    </section>
</div><!--end body-->
<?php include_once('footer.php'); ?>
   